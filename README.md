# 0 Workshop Overview

In this workshop you'll get a sense of what it's like to generate and run calculations on a larger scale molecular library.

It is divided in three sections:

1. **Introduction to IPython notebooks**, the tool we will be using to compute and visualize data.
2. **Calculating molecular properties with PyQuante**, a python-based electronic structure calculation module.
3. **Molecular Library Screening with RDKit**, we'll pretend that we're building and screening a library of organic molecules for photovoltaics applications.

![](http://aspuru.chem.harvard.edu/blog/wp-content/uploads/2012/02/CEPicon.jpg)

# Requirements for the Workshop:

* Python 2.7.x
* Ipython notebook > 2.4x or 3.x
* rdkit
* openbabel
* Modules: scipy, numpy, matplotlib, pandas, openbabel, imolecule, PyQuante, pillow

# Starting
Fortunately we will be using a VirtualBox disk image of Ubuntu 14.04.2 loaded with all the programs.

In case you need it, the password for the machines is : **QuantumW**

To start:

1. Open a terminal at desktop by right clicking on the desktop and highlight open terminal on the menu.
2. Run the workshop update script by typing in the terminal **./update_workshop.sh**.
3. When done enter the directory by typing **cd mole_screen_workshop**.
4. Type in your terminal **ipython notebook**, this will open an ipython notebook in google chrome.
5. Open the first notebook **1_Computaton_with_Ipython_Noteboks.ipynb**.

# **Extra:** Want to install ipython notebook in your computer?

A good resource is the official ipython website:

[![](http://ipython.org/_static/IPy_header.png)](http://ipython.org/install.html)

## How to install in Ubuntu (Linux)?

```
sudo apt-get update
sudo apt-get install python-dev python-pip gfortran liblapacke-dev liblapack-dev libatlas-dev libpython-all-dev librdkit-dev python-rdkit openbabel python-openbabel libpng12-dev libfreetype6-dev

sudo  pip install scipy imolecule pandas matplotlib pillow
sudo pip install "ipython[notebook]"
```

## How to install in your Mac?

This is the sequence of commands that got me there...
** Install Homebrew**
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
then all the rest
```
sudo easy_install pip

brew tap mcs07/cheminformatics
brew install Caskroom/cask/xquartz
brew install mcs07/cheminformatics/open-babel --HEAD
brew install  mcs07/cheminformatics/rdkit --HEAD

sudo  pip install scipy imolecule pandas matplotlib pillow openbabel
sudo pip install "ipython[notebook]"

wget http://downloads.sourceforge.net/project/pyquante/PyQuante-1.6/PyQuante-1.6.5/PyQuante-1.6.5.tar.gzr=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fpyquante%2F&ts=1432823385&use_mirror=softlayer-dal
tar xzvf PyQuante-1.6.5.tar.gz
cd PyQuante-1.6.5
sudo python setup.py install

# run the iPython notebook and feel like a pro!
ipython notebook &
```
## Windows?

Your best bet is to install all the packages via anaconda:

[![](https://store.continuum.io/static/img/anaconda_logo_web.png)](http://continuum.io/downloads#all)